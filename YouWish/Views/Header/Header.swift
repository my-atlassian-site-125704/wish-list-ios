//
//  Header.swift
//  YouWish
//
//  Created by Anton Eliseev on 01.11.2021.
//

import SwiftUI

struct Header: View {
    var body: some View {
        HStack {
            Image("Avatar")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: 64)
                        .clipShape(Circle())
            VStack(alignment: .leading) {
                Text("Илья Н.")
                    .bold()
                    .font(Font.system(size: 20, design: .rounded))
                Text("5 желаний")
                    .font(Font.system(size: 15, design: .rounded))
                    .foregroundColor(Color.secondary)
            }
            Spacer()
            if #available(iOS 15.0, *) {
                Button(action: {
                    print("Delete tapped!")
                }){
                    Text("•••")
                }
                .frame(width: 34, height: 34)
                //                .background(Color("MoreButton"))
                .background(Color.indigo.opacity(0.1))
                .cornerRadius(20)
            } else {
                // Fallback on earlier versions
            }
            
        }.padding(28)
    }
}

struct Header_Previews: PreviewProvider {
    static var previews: some View {
        Header()
//        Header().preferredColorScheme(.dark)
    }
}
