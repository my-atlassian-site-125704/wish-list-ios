//
//  WishList.swift
//  YouWish
//
//  Created by Anton Eliseev on 07.11.2021.
//

import SwiftUI

struct ListItem: Identifiable {
    let id = UUID()
    let imgString: String
    
    init(imgString: String){
        self.imgString = imgString
    }
}

struct WishList: View {
    
    let spacing = CGFloat(12)
    let listItems: [ListItem]
    
    init(listItems: [ListItem]) {
        self.listItems = listItems
    }
    
    var body: some View {
        LazyVStack(alignment: .leading, spacing: spacing) {
            ForEach(listItems) { listItem in
                
                WishListItem(listItem: listItem)
                    .padding(spacing)
                    .background(Color.white)//.padding(16)
                    .cornerRadius(20)
            }
        }
        .padding(.horizontal, spacing)
        .background(Color(red: 242 / 255.0, green: 242 / 255.0, blue: 247.0 / 255.0))
    }
    
    func WishListItem(listItem: ListItem) -> some View {
        HStack(spacing: spacing) {
            Image(listItem.imgString)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 54, height: 54)
                .clipShape(RoundedRectangle(cornerRadius: 10, style: .circular))
            VStack(alignment: .leading) {
                Text(listItem.imgString).font(.body)
                Text("Какое-то длинное описание").font(.subheadline).foregroundColor(.secondary)
            }
            Spacer()
            VStack {
                Image(systemName: "link.circle.fill")
                    .resizable()
                    .frame(width: 16, height: 16)
                    .foregroundColor(Color.green)
    //                            .background(Color.green)
    //                            .clipShape(Circle())
    //                            .overlay(Circle().stroke(Color.red, lineWidth: 0))
                Text("\(Int.random(in: 20...999))$").font(.body).foregroundColor(.secondary)
            }
        }
    }
}

struct WishList_Previews: PreviewProvider {
    static var previews: some View {
        MainScreenList()
    }
}
