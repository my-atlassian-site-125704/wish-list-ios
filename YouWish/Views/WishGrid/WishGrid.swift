//
//  WishGrid.swift
//  YouWish
//
//  Created by Anton Eliseev on 01.11.2021.
//

import SwiftUI

struct GridItem: Identifiable {
    let id = UUID()
    let height: CGFloat
    let imgString: String
    let img: Image
    
    init(imgString: String) {
        let img = UIImage(named: imgString)!
        let originImgWidth: CGFloat = img.size.width
        let originImgHeight: CGFloat = img.size.height
        
        let realImgWidth = UIScreen.main.bounds.width / 2
        let scale = originImgWidth / realImgWidth
        let realImgHeight = originImgHeight / scale
        
        self.imgString = imgString;
        self.height = realImgHeight
        self.img = Image(uiImage: img)
        // print("imgWidth: \(realImgWidth) / \(realImgHeight)")
    }
}

struct Column: Identifiable {
    let id = UUID()
    var gridItems = [GridItem]()
}

struct WishGrid: View {
    
    let columns: [Column]
//    [
//        Column(gridItems: [
//            GridItem(height: CGFloat.random(in: 20 ... 200), title: "1"),
//            GridItem(height: CGFloat.random(in: 20 ... 200), title: "3"),
//            GridItem(height: CGFloat.random(in: 20 ... 200), title: "5"),
//            GridItem(height: CGFloat.random(in: 20 ... 200), title: "7"),
//        ]),
//        Column(gridItems: [
//            GridItem(height: CGFloat.random(in: 20 ... 200), title: "2"),
//            GridItem(height: CGFloat.random(in: 20 ... 200), title: "4"),
//            GridItem(height: CGFloat.random(in: 20 ... 200), title: "6"),
//            GridItem(height: CGFloat.random(in: 20 ... 200), title: "8"),
//        ])
//    ]
    
    let spacing: CGFloat
    let horizontalPadding: CGFloat
    
    init(gridItems: [GridItem], numOfColumns: Int, spacing: CGFloat = 10, horizontalPadding: CGFloat = 10) {
        self.spacing = spacing
        self.horizontalPadding = horizontalPadding
        
        var columns = [Column]()
        for _ in 0 ..< numOfColumns {
            columns.append(Column())
        }
        
        var columnsHeight = Array<CGFloat>(repeating: 0, count: numOfColumns)
        
        
        for gridItem in gridItems {
            var smallestColIndex = 0
            var smallestHeight = columnsHeight.first!
            for i in 1 ..< columnsHeight.count {
                let curHeight = columnsHeight[i]
                if curHeight < smallestHeight {
                    smallestHeight = curHeight
                    smallestColIndex = i
                }
            }
            
            columns[smallestColIndex].gridItems.append(gridItem)
            columnsHeight[smallestColIndex] += gridItem.height
        }
        
        self.columns = columns
    }
    
    var body: some View {
        HStack(alignment: .top, spacing: spacing) {
            ForEach(columns) { column in
                LazyVStack(spacing: spacing) {
                    ForEach(column.gridItems) { gridItem in
                        getItemView(gridItem: gridItem)
                    }
                }
            }
        }.padding(.horizontal, horizontalPadding)
    }
    
    func getItemView(gridItem: GridItem) -> some View {
        VStack(alignment: .leading) {
            ZStack{
                GeometryReader { render in
                    //Image(gridItem.imgString)
                    gridItem.img
                        .resizable()
                        .aspectRatio(contentMode: .fill)
    //                    .scaledToFit()
                        .frame(width: render.size.width, height: render.size.height, alignment: .center)
                    
                }
            }
            .frame(height: gridItem.height)
            .frame(maxWidth: .infinity)
            .clipShape(RoundedRectangle(cornerRadius: 10))
            Text(gridItem.imgString).font(.body)
            Text("\(Int.random(in: 20...999))$").font(.body).foregroundColor(.secondary)
        }
    }
}

struct WishGrid_Previews: PreviewProvider {
    static var previews: some View {
        MainScreen()
    }
}
