//
//  WishGridItom.swift
//  YouWish
//
//  Created by Anton Eliseev on 01.11.2021.
//

import SwiftUI

struct WishGridItem: View {
    var body: some View {
        VStack(alignment: .leading){
            Image("GooglePixel")
                .resizable()
//                .aspectRatio(contentMode: .fit)
                .scaledToFill()
                .frame(
                    width: UIScreen.main.bounds.width / 2,
                    height: UIScreen.main.bounds.width / 2)
                .cornerRadius(20)
                .clipped()
//                .clipShape(Circle())
            Text("Google Pixel 4a 5G 64Gb").font(.body)
            Text("456$").font(.body).foregroundColor(.secondary)
        }.border(Color.green)
    }
}

struct WishGridItom_Previews: PreviewProvider {
    static var previews: some View {
        WishGridItem()
    }
}
