//
//  WishGrid.swift
//  YouWish
//
//  Created by Anton Eliseev on 01.11.2021.
//

import SwiftUI

struct WishGrid2: View {
    
    @ObservedObject var controller = WishGridController();
    
    var body: some View {
        if controller.leftItems.count == 0 {
            ProgressView()
                .onAppear {
                    controller.loadGrid()
                }
        } else {
            ScrollView {
                HStack {
                    VStack(spacing: 8) {
                        Text("L0")
                        ForEach(0..<controller.leftItems.count , id: \.self) { i in
                            Text("lll")
                            controller.leftItems[i]
                        }
                    }
                    VStack(spacing: 8) {
                        Text("R0")
                        ForEach(0..<controller.rightItems.count , id: \.self) { i in
                            Text("rrr")
                            controller.rightItems[i]
                        }
                    }
                }
            }
        }
    }
}

struct WishGrid2_Previews: PreviewProvider {
    static var previews: some View {
        WishGrid2()
    }
}
