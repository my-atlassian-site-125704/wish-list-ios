//
//  Loader.swift
//  YouWish
//
//  Created by Anton Eliseev on 02.11.2021.
//

import SwiftUI
import Foundation



class WishGridController: ObservableObject {
    
    var leftHeight: CGFloat = CGFloat(0);
    var righHeight: CGFloat = CGFloat(0);
    
    @Published var leftItems: [AnyView] = []
    @Published var rightItems: [AnyView] = []
    
    func loadGrid() {
        for _ in 0..<10 {
            var av: AnyView
            print("Set leftHeight / righHeight: \(leftHeight) / \(righHeight)")
            if(leftHeight <= righHeight){
                av = AnyView(self.lgr());
                leftItems.append(av)
            } else {
                av = AnyView(self.rgr());
                rightItems.append(av)
            }
        }
    }
    
    
    func lgr() -> some View {
        return GeometryReader { geometry in
            self.saveHeight(dir: "left", height: geometry.size.height)
        }
    }
    
    func rgr() -> some View {
        return GeometryReader { geometry in
            self.saveHeight(dir: "right", height: geometry.size.height)
        }
    }
    
    
    func saveHeight(dir: String, height: CGFloat) -> some View {
        
        if(dir == "left"){
            print("Set leftHeight: \(leftHeight) += \(height)")
            self.leftHeight += height
        } else {
            print("Set righHeight: \(righHeight) += \(height)")
            self.righHeight += height
        }
        
        let height1 = Int.random(in: 40..<100)
        return Rectangle()
            .fill(Color.green)
            .frame(width: UIScreen.main.bounds.width / 2 - 16, height: CGFloat(height1))
            .padding(8)

    }

    

}
