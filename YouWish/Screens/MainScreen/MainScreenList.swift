//
//  MainScreenList.swift
//  YouWish
//
//  Created by Anton Eliseev on 07.11.2021.
//

import SwiftUI

struct MainScreenList: View {
    
    @State private var isShowingDetailView = false
    
//    let gridItems = [
//        GridItem(imgString: "Dyson Supersonic"),
//        GridItem(imgString: "Google Pixel 4A"),
//        GridItem(imgString: "Google Pixel Buds"),
//        GridItem(imgString: "Dyson Supersonic"),
//        GridItem(imgString: "Apple Pencil 2"),
//        GridItem(imgString: "Google Pixel 4A"),
//        GridItem(imgString: "Google Pixel Buds"),
//        GridItem(imgString: "Apple Pencil 2"),
//        GridItem(imgString: "Google Pixel 4A"),
//        GridItem(imgString: "Dyson Supersonic"),
//    ]
    
        let listItems = [
            ListItem(imgString: "Dyson Supersonic"),
            ListItem(imgString: "Google Pixel 4A"),
            ListItem(imgString: "Google Pixel Buds"),
            ListItem(imgString: "Dyson Supersonic"),
            ListItem(imgString: "Apple Pencil 2"),
            ListItem(imgString: "Google Pixel 4A"),
            ListItem(imgString: "Google Pixel Buds"),
            ListItem(imgString: "Apple Pencil 2"),
            ListItem(imgString: "Google Pixel 4A"),
            ListItem(imgString: "Dyson Supersonic"),
            ListItem(imgString: "Google Pixel Buds"),
            ListItem(imgString: "Apple Pencil 2"),
            ListItem(imgString: "Google Pixel 4A"),
            ListItem(imgString: "Dyson Supersonic"),
        ]
    
    func PrifileIcon() -> some View {
        Image("Avatar")
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(height: 32)
            .clipShape(Circle())
    }
    
    var body: some View {
//        var gridItems = [GridItem]()
//        for i in 0 ..< 30 {
//            let rndHeight = CGFloat.random(in: 100 ... 400)
//            gridItems.append(GridItem(height: rndHeight, title: String(i)))
//        }
        
        return NavigationView {
            VStack{
                ScrollView {
//                    WishGrid(gridItems: gridItems, numOfColumns: 2)
                    HStack {
                        Text("My wishes").font(.largeTitle).bold()
                        Spacer()
                        PrifileIcon()
                    }.padding(16)
                    WishList(listItems: listItems)
                }//.background(Color.gray.opacity(0.25))
                Button(action: {}){
                    HStack {
                        Image(systemName: "plus")
                            .resizable()
                            .frame(width: 16, height: 16)
                            .foregroundColor(.white)
                        Text("Add new")
                            //.frame(width: UIScreen.main.bounds.width - 20)

                    }
                    .padding(.horizontal, 16)
                    .padding(.vertical, 12)
                }
                .foregroundColor(Color.white)
                .background(Color.green)
                .cornerRadius(16)
            }
            //.navigationTitle("Желания") //(Header()) // "Some header"
                .navigationBarTitleDisplayMode(.inline)
            .toolbar(content: {
                ToolbarItem(placement: .principal) {
                    Text("My wishes")
                }
                ToolbarItem(placement: .primaryAction) {
                    PrifileIcon()
                }
            })
        }
    }
}

struct MainScreenList_Previews: PreviewProvider {
    static var previews: some View {
        MainScreenList()
    }
}
