//
//  ContentView.swift
//  YouWish
//
//  Created by Anton Eliseev on 01.11.2021.
//

import SwiftUI

struct MainScreen: View {
    
//    let gridItems = [
//        GridItem(imgString: "Dyson Supersonic"),
//        GridItem(imgString: "Google Pixel 4A"),
//        GridItem(imgString: "Google Pixel Buds"),
//        GridItem(imgString: "Dyson Supersonic"),
//        GridItem(imgString: "Apple Pencil 2"),
//        GridItem(imgString: "Google Pixel 4A"),
//        GridItem(imgString: "Google Pixel Buds"),
//        GridItem(imgString: "Apple Pencil 2"),
//        GridItem(imgString: "Google Pixel 4A"),
//        GridItem(imgString: "Dyson Supersonic"),
//    ]
    
        let listItems = [
            ListItem(imgString: "Dyson Supersonic"),
            ListItem(imgString: "Google Pixel 4A"),
            ListItem(imgString: "Google Pixel Buds"),
            ListItem(imgString: "Dyson Supersonic"),
            ListItem(imgString: "Apple Pencil 2"),
            ListItem(imgString: "Google Pixel 4A"),
            ListItem(imgString: "Google Pixel Buds"),
            ListItem(imgString: "Apple Pencil 2"),
            ListItem(imgString: "Google Pixel 4A"),
            ListItem(imgString: "Dyson Supersonic"),
        ]
    
    var body: some View {
//        var gridItems = [GridItem]()
//        for i in 0 ..< 30 {
//            let rndHeight = CGFloat.random(in: 100 ... 400)
//            gridItems.append(GridItem(height: rndHeight, title: String(i)))
//        }
        
        // Header()
        
        return NavigationView {
            VStack{
                ScrollView {
//                    WishGrid(gridItems: gridItems, numOfColumns: 2)
                    WishList(listItems: listItems)
                }
                Button(action: {}){
                    Text("Добвить желание")
                        .frame(width: UIScreen.main.bounds.width - 20)
//                        .padding(.horizontal, 32)
                        .padding(.vertical, 16)
                }
                .foregroundColor(Color.white)
                .background(Color.purple)
                .cornerRadius(10)
            }.navigationTitle("Желания") //(Header()) // "Some header"
                // .navigationBarTitleDisplayMode(.inline)
            .toolbar(content: {
                ToolbarItem(placement: .navigationBarLeading) {
                    Image("Avatar")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(height: 32)
                        .clipShape(Circle())
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                            //removeTask()
                    }) {
                        Image(systemName: "ellipsis.circle").imageScale(.large)
                    }
//                    .frame(width: 34, height: 34, alignment: .center)
//                    .background(Color.purple.opacity(0.1))
//                    .cornerRadius(20) // 14:30
                }
            })
        }
    }
}

struct MainScreen_Previews: PreviewProvider {
    static var previews: some View {
        MainScreen()
    }
}
