//
//  ScrollMagic.swift
//  YouWish
//
//  Created by Anton Eliseev on 25.11.2021.
//

import SwiftUI

struct ScrollMagic: View {
    
    private struct OffsetKey: PreferenceKey {
        static var defaultValue: CGFloat = 0
        static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
            value = nextValue()
        }
    }
    
    @State var onTop = true
    
    var scrollView: some View {
        GeometryReader { g in
            ScrollView(.vertical) {
                VStack {
                    ForEach(0...20, id: \.self) { count in
                        (count % 2 == 0 ? Color.red : Color.blue)
                            .frame(height: 44.0)
                    }
                }
                .padding(.top, 60)
                .padding(.bottom, 50)
                .anchorPreference(key: OffsetKey.self, value: .top) {
                    g[$0].y
                }
            }
        }
    }
    
    var topBar: some View {
        VStack {
            Text("Title Bar")
                .font(.largeTitle)
            Text("On top ☝️")
        }
        .frame(maxWidth: .infinity)
        .padding(.top, 35)
        .background(Color(UIColor.systemBackground))
    }
    
    var bottomBar: some View {
        (Text("Title Bar").bold() + Text(" at the bottom 👇"))
            .frame(maxWidth: .infinity)
            .padding()
            .background(Color(UIColor.systemBackground))
            .opacity(onTop ? 0 : 0.85)
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            Color.green.frame(height: 150.0).padding(.top, 0)
            scrollView
                .onPreferenceChange(OffsetKey.self) {
                    if $0 < -10 {
                        withAnimation {
                            self.onTop = false
                        }
                    } else {
                        withAnimation {
                            self.onTop = true
                        }
                    }
                }
            VStack {
                //        if onTop {
                //          topBar
                //          .transition(.move(edge: .top))
                //        }
                //        Spacer()
                //        bottomBar
            }
            .edgesIgnoringSafeArea([.top, .bottom])
        }
    }
}


struct MyHeader: View {
    var body: some View {
        Text("Header")
            .padding()
            .frame(maxWidth: .infinity)
    }
}

struct DemoTableHeader: View {
    let myList: [String] = ["1", "2", "3"]

    let headerHeight = CGFloat(24)

    var body: some View {
        ZStack(alignment: .topLeading) {
            MyHeader().zIndex(1)               // << header
                .frame(height: headerHeight)

            List {
                Color.clear                    // << under-header placeholder
                    .frame(height: headerHeight)

                ForEach(myList, id: \.self) { element in
                    Text(element)
                }
            }
        }
    }
}

struct ScrollMagic_Previews: PreviewProvider {
    static var previews: some View {
        DemoTableHeader()
    }
}
