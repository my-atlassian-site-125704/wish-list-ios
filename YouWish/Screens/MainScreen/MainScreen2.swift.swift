//
//  MainScreen2.swift.swift
//  YouWish
//
//  Created by Anton Eliseev on 24.11.2021.
//

import SwiftUI

struct NavigationConfigurator: UIViewControllerRepresentable {
    var configure: (UINavigationController) -> Void = { _ in }

    func makeUIViewController(context: UIViewControllerRepresentableContext<NavigationConfigurator>) -> UIViewController {
        UIViewController()
    }
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<NavigationConfigurator>) {
        if let nc = uiViewController.navigationController {
            self.configure(nc)
        }
    }

}

struct MainScreen2_swift: View {
    //@State var startOffset: CGFloat = 0;
    @State var lastOffset: CGFloat = 0;
    @State var scrollOffset: CGFloat = 0;
    
    func getSafeArea() -> UIEdgeInsets {
        return UIApplication.shared.windows.first?.safeAreaInsets ?? UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    var body: some View {
        VStack(spacing: 0) {
            Color.green.frame(height: 150.0).offset(y: scrollOffset - 150 - getSafeArea().top)
            ScrollView {
                VStack {
                    ForEach(0...20, id: \.self) { count in
                        (count % 2 == 0 ? Color.red : Color.blue)
                            .frame(height: 44.0)
                    }
                }.cornerRadius(12)
                .overlay(
                    GeometryReader{ proxy -> Color in
        
                        DispatchQueue.main.async {
                            self.lastOffset = scrollOffset
        
//                            if(startOffset == 0) {
//                                self.startOffset = proxy.frame(in: .global).midY
//                            }
//
//                            let offset = proxy.frame(in: .global).midY
//                            self.scrollOffset = offset - startOffset
                            self.scrollOffset = proxy.frame(in: .global).midY
                            print(scrollOffset - lastOffset > 0 ? "DOWN" : "UP");
//                            print(scrollOffset - lastOffset)
                        }
                        return Color.clear;
                    }.frame(width: 0, height: 0)
                    , alignment: .top
                )
            }
        }

    }
}

struct MainScreen2_swift_Previews: PreviewProvider {
    static var previews: some View {
        MainScreen2_swift()
    }
}
