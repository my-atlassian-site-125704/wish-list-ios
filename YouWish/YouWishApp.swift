//
//  YouWishApp.swift
//  YouWish
//
//  Created by Anton Eliseev on 01.11.2021.
//

import SwiftUI

@main
struct YouWishApp: App {
    var body: some Scene {
        WindowGroup {
            ScrollMagic()
        }
    }
}
